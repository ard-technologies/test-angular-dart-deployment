import 'package:angular/angular.dart';
import 'package:angular_app/src/models/hero.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_forms/angular_forms.dart';

const List<String> _powers = [
  'Really Smart',
  'Super Flexible',
  'Super Hot',
  'Weather Changer'
];

@Component(
  selector: 'hero-form',
  templateUrl: 'hero_form_component.html',
  styleUrls: ['package:angular_components/css/mdc_web/card/mdc-card.scss.css',
  'hero_form_component.css'],
  directives: [
    coreDirectives,
    formDirectives,
    MaterialIconComponent,
    MaterialInputComponent,
    materialInputDirectives,
  ],
)
class HeroFormComponent {
  ErrorFn myErrors = replaceErrors(
      {'lower-bound-number': 'Bigger number please',
        'upper-bound-number': 'Smaller number please'});
  Hero model = Hero(18, 'Dr IQ', _powers[0], 'Chuck Overstreet');
  bool submitted = false;

  List<String> get powers => _powers;

  void onSubmit() => submitted = true;

  void clear() {
    model.name = '';
    model.power = _powers[0];
    model.alterEgo = '';
  }

  Map<String, bool> setCssValidityClass(NgControl control) {
    final validityClass = control.valid == true ? 'is-valid' : 'is-invalid';
    return {validityClass: true};
  }
}
