import 'package:angular/angular.dart';
import 'package:angular_app/src/models/hero.dart';
import 'package:angular_app/src/resources/hero_service.dart';
import 'package:angular_app/src/ui/routing/route_paths.dart';
import 'package:angular_router/angular_router.dart';

@Component(
  selector: 'my-dashboard',
  templateUrl: 'dashboard_component.html',
  styleUrls: ['dashboard_component.css'],
  directives: [coreDirectives,routerDirectives],
)
class DashboardComponent implements OnInit {
  List<Hero> heroes;

  final HeroService _heroService;

  DashboardComponent(this._heroService);

  @override
  void ngOnInit() async {
    heroes = (await _heroService.getAll()).skip(1).take(4).toList();
  }
  String heroUrl(int id) => RoutePaths.hero.toUrl(parameters: {idParam: '$id'});
}