import 'package:angular_router/angular_router.dart';

import '../herolist/hero_list_component.template.dart' as hero_list_template;
import '../dashboard/dashboard_component.template.dart' as dashboard_template;
import '../hero/hero_component.template.dart' as hero_template;
import '../heroform/hero_form_component.template.dart' as hero_form_template;
import 'route_paths.dart';

export 'route_paths.dart';

class Routes {
  static final heroes = RouteDefinition(
    routePath: RoutePaths.heroes,
    component: hero_list_template.HeroListComponentNgFactory,
  );

  static final hero = RouteDefinition(
    routePath: RoutePaths.hero,
    component: hero_template.HeroComponentNgFactory,
  );

  static final dashboard = RouteDefinition(
    routePath: RoutePaths.dashboard,
    component: dashboard_template.DashboardComponentNgFactory,
  );
  static final heroForm = RouteDefinition(
    routePath: RoutePaths.heroForm,
    component: hero_form_template.HeroFormComponentNgFactory,
  );

  static final all = <RouteDefinition>[
    RouteDefinition.redirect(
      path: '',
      redirectTo: RoutePaths.heroForm.toUrl(),
    ),
    heroForm,
    heroes,
    hero,
    dashboard,
  ];
}
