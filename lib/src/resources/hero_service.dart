import 'package:angular_app/src/models/hero.dart';


class HeroService{
  static final mockHeroes = <Hero>[
//    Hero(11, 'Mr. Nice'),
//    Hero(12, 'Narco'),
//    Hero(13, 'Bombasto'),
//    Hero(14, 'Celeritas'),
//    Hero(15, 'Magneta'),
//    Hero(16, 'RubberMan'),
//    Hero(17, 'Dynama'),
//    Hero(18, 'Dr IQ'),
//    Hero(19, 'Magma'),
//    Hero(20, 'Tornado')
  ];

  Future<List<Hero>> getAll() async => mockHeroes;

  Future<Hero> get(int id) async =>
      (await getAll()).firstWhere((hero) => hero.id == id);

  Future<List<Hero>> getAllSlowly() {
    return Future.delayed(Duration(seconds: 2), getAll);
  }
}
