import 'package:angular/angular.dart';
import 'package:angular_app/src/resources/hero_service.dart';
import 'package:angular_app/src/ui/routing/route_paths.dart';
import 'package:angular_app/src/ui/routing/routes.dart';
import 'package:angular_router/angular_router.dart';

@Component(
  selector: 'my-app',
  templateUrl: 'app_component.html',
  styleUrls: ['app_component.css'],
  directives: [routerDirectives],
  providers: [ClassProvider(HeroService)],
  exports: [RoutePaths, Routes],
)
class AppComponent {
  final title = 'Tour of Heroes';


}
